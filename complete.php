<html>
<head>
	<title>Pantana Accounting &amp; Tax - Pay Online</title>
	<meta charset="utf-8">
	<meta name="description" content="Online payment page for clients of Pantana Accounting & Tax.">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
<img src="logowhitecolor_04.png" alt="Pantana Accounting Logo" class="logo">
<div class="container">

<? if ($_GET["transactionId"] == ""): ?>
	<h1 class="error">Problem with Payment</h1>

	<p>We weren't able to process that payment. 

 	<?if ($_GET["errorMsg"] != ""):?>
	Here is the error provided by the payment processor:
	<br><br><b><?=strip_tags($_GET["errorMsg"])?></b>
	<? else: ?>
	Unfortunately no details were provided by the payment processor on why that transaction didn't go through.
	<? endif ?>
	</p>

	<p><a href="/">Try payment again</a> or contact us at (678) 213-0602.</p>

<? else: ?>

<h1>Thank you!</h1>
<p>Your payment has been processed. Here is your confirmation number:</p>
<p><b>	<?=strip_tags($_GET["transactionId"])?></b></p>

<p>We appreciate your business!<br> &mdash;Pantana Accounting &amp; Tax </p>

<p><a href="http://pantana-cpa.com">Pantana Accounting homepage &gt;</a></p>
<? endif ?>

</div>

</body>
</html>