<?
include('SimpleToken.php');
$key = '5f7ddb1bd6d98df8320146e319879fe6';
$content = "PantanaPaymentForm";
$token = SimpleToken::generateToken($key, $content);


if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $redirect);
    exit();
}

$invoice_name = isset($_COOKIE["invoice_name"]) ? $_COOKIE["invoice_name"] : "";
$invoice_email = isset($_COOKIE["invoice_email"]) ? $_COOKIE["invoice_email"] : "";

// $time = time();
// $_SESSION['time'] = $time;
// $salt = 'D2A88B5EFB4E3EA00919D44A0EE70098C26389EE567869FC381F5AB11686D50E';
// $token = sha1($salt . $time);
?>

<html>
<head>
	<title>Pantana Accounting &amp; Tax - Pay Online</title>
	<meta charset="utf-8">
	<meta name="description" content="Online payment page for clients of Pantana Accounting & Tax.">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<link href="style.css" rel="stylesheet" type="text/css">
	<!-- <script src='https://www.google.com/recaptcha/api.js'></script> -->
</head>
<body>
<img src="logowhitecolor_04.png" alt="Pantana Accounting Logo" class="logo">
<div class="container">
<h1>Pay Online&mdash;Pantana Accounting &amp; Tax </h1>

<form method="post" action="chargecard.php">
<!-- <h2>Invoice Info</h2> -->
<input type="hidden" name="token" value="<?php echo $token; ?>" />
<label for="name">Payer Name (on Invoice)</label>
<input id="name" name="name" value="<?=$invoice_name?>" placeholder="Name" required>

<label for="email">Email Address</label>
<input id="email" name="email" value="<?=$invoice_email?>" placeholder='your@email.com' required>

<label for="invoice">Invoice Number(s) <em>Optional</em></label>
<input id="invoice" name="invoice" placeholder="ex: 1234">


<label for="amount">Amount</label>
<div class="amount-wrapper">
<input id="amount" name="amount" class="amount" type="number" required>
</div>

<label for="card">Credit Card #</label> 
<input id="card" name="card" value="" required maxlength="16">


<label for="exp">Expiration Date</label>
<div class="exp">
<select name="expmonth" id="expmonth">
	<option value="01">1</option>
	<option value="02">2</option>
	<option value="03">3</option>
	<option value="04">4</option>
	<option value="05">5</option>
	<option value="06">6</option>
	<option value="07">7</option>
	<option value="08">8</option>
	<option value="09">9</option>
	<option value="10">10</option>
	<option value="11">11</option>
	<option value="12">12</option>
</select>

<select name="expyear" id="expyear">
	 <?php 
    for ($i = date('Y'); $i <= date('Y')+10; $i++) {
        echo '<option value="'.$i.'">'.$i.'</option>';
    } ?>
</select>
</div>

<!-- <button class="g-recaptcha" data-sitekey="6LdasR8UAAAAAIM7pu2DZCR3CDXWh-piJbteKnIG" data-callback="">
Submit
</button> -->

<button type="submit" id="submit">Submit Payment</button>

<label for="remember" class="checkbox"><input type="checkbox" name="remember" value="1" id="remember" checked> Remember my name &amp; email</label>
					
</form>

</div>

<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="
  crossorigin="anonymous"></script>
  <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js"></script>

<script>

//$("#name").val(getCookie("name"));
//$("#email").val(getCookie("email"));

$("form").validate();

$("form").submit(function() {
	// if ($("#remember").is(":checked")) {
	// 	setCookie("name", $("#name").val(), 365);
	// 	setCookie("email", $("#email").val(), 365);
	// } else {
	// 	setCookie("name", "", 2);
	// 	setCookie("email", "", 2);
	// }

	if ($("form").valid()) {
		$("#submit").attr("disabled","disabled").text("Processing Payment...");
	}
});


function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

</script>
</body>
</html>