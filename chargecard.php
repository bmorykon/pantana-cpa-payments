<?
// session_start();
// $salt = 'D2A88B5EFB4E3EA00919D44A0EE70098C26389EE567869FC381F5AB11686D50E';
// $token = sha1($salt . $_SESSION['time']);

//set cookies
$cookie1_name = "invoice_name";
$cookie2_name = "invoice_email";

if ($_POST['remember']==1) {  
  $cookie1_value = $_POST["name"];
  $cookie2_value = $_POST["email"];
} else {
  $cookie1_value = "";
  $cookie2_value = "";
}

setcookie($cookie1_name, $cookie1_value, time() + (86400 * 90), "/");
setcookie($cookie2_name, $cookie2_value, time() + (86400 * 90), "/");

// echo $token;
// die();

include('SimpleToken.php');
$key = '5f7ddb1bd6d98df8320146e319879fe6';
$content = "PantanaPaymentForm";

if(!SimpleToken::isAuthentic($key, $content, $_POST["token"])) {
die('Invalid security token');
}
//Rest of form validation

require 'vendor/autoload.php'; 
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
define("AUTHORIZENET_LOG_FILE", "phplog");

// Common setup for API credentials
$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();

// BRIAN SANDBOX CREDS
// $merchantAuthentication->setName("5h28QkaPe");
// $merchantAuthentication->setTransactionKey("2YgK3mFv7J6p295z");

// PANTANA API CREDS
$merchantAuthentication->setName("6C85W7wpW");
$merchantAuthentication->setTransactionKey("35sXL8x4wVt87A35"); 



 // Create the payment data for a credit card
    $creditCard = new AnetAPI\CreditCardType();
    $creditCard->setCardNumber($_POST["card"]);
    $creditCard->setExpirationDate($_POST["expyear"] . "-" . $_POST["expmonth"]);
    //$creditCard->setCardCode("123");
    $paymentOne = new AnetAPI\PaymentType();
    $paymentOne->setCreditCard($creditCard);

    $order = new AnetAPI\OrderType();
    //$order->setDescription("New Item");
    $order->setInvoiceNumber($_POST["invoice"]);

    // Set the customer's Bill To address
    $customerAddress = new AnetAPI\CustomerAddressType();
    $customerAddress->setFirstName($_POST["name"]);
    //$customerAddress->setLastName("Johnson");
    $customerAddress->setCompany($_POST["name"]);
    $customerAddress->setEmail($_POST["email"]);
    // $customerAddress->setAddress("14 Main Street");
    // $customerAddress->setCity("Pecan Springs");
    // $customerAddress->setState("TX");
    // $customerAddress->setZip("44628");
    // $customerAddress->setCountry("USA");

    $customer = new AnetAPI\CustomerDataType();
    $customer->setEmail($_POST["email"]);


    // Create a TransactionRequestType object
    $transactionRequestType = new AnetAPI\TransactionRequestType();
    $transactionRequestType->setTransactionType("authCaptureTransaction"); 
    $transactionRequestType->setAmount($_POST["amount"]);
    $transactionRequestType->setOrder($order);
    $transactionRequestType->setPayment($paymentOne);
    $transactionRequestType->setBillTo($customerAddress);
    $transactionRequestType->setCustomer($customer);

    $request = new AnetAPI\CreateTransactionRequest();
    $request->setMerchantAuthentication($merchantAuthentication);
    $request->setRefId( $refId);
    $request->setTransactionRequest( $transactionRequestType);

    $controller = new AnetController\CreateTransactionController($request);
    $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION); //SANDBOX

  /*  $sendRequest = new net\authorize\api\contract\v1\SendCustomerTransactionReceiptRequest();
    $sendRequest->setTransId($sendRequest->transactionResponse->TransID);
    $sendRequest->setCustomerEmail($_POST["email"]);
    $sendRequest->setMerchantAuthentication($merchantAuthentication);

    $sendController = new net\authorize\api\controller\SendCustomerTransactionReceiptController($sendRequest);
    $sendResponse = $sendController->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX); //SANDBOX */
    

$transactionID = "";
$success = false;
$errorMsg = "";
//print_r($response->getMessages()->getResultCode());
        //die();

// No response
if ($response == null) {
    header( 'Location: /complete.php');
    exit();
}

$tresponse = $response->getTransactionResponse();

// All is good
if($response->getMessages()->getResultCode() == 'Ok') {
            header( 'Location: /complete.php?transactionId=' . $tresponse->getTransId());
            exit();
}

// Error
if ($response->getMessages()->getResultCode() == 'Error') {

    //
    if ($tresponse != null) { 

        // no error message
        if ($tresponse->getErrors()[0] == null)  {
            header( 'Location: /complete.php');
            exit(); 
        }

        // there's an error message, so show it
        header('Location: /complete.php?errorMsg=' . $tresponse->getErrors()[0]->getErrorText());
        //. ' Code: ' . $tresponse->getErrors()[0]->getErrorCode());  
        exit();   
    }
}

// Somehow it got here which means there was an uncaught error and transaction failed
header( 'Location: /complete.php');


/* ---------------------
}
    {
      if($response->getMessages()->getResultCode() == 'Ok' || $response->getMessages()->getResultCode() == 'Error')
      {
        $tresponse = $response->getTransactionResponse();

     //   print_r($tresponse);
      //  die();
        
        if ($tresponse != null && $tresponse->getMessages() != null)   
        {
         // echo " Transaction Response code : " . $tresponse->getResponseCode() . "\n";
         // echo "Charge Credit Card AUTH CODE : " . $tresponse->getAuthCode() . "\n";
         // echo "Charge Credit Card TRANS ID  : " . $tresponse->getTransId() . "\n";
         // echo " Code : " . $tresponse->getMessages()[0]->getCode() . "\n"; 
         // echo " Description : " . $tresponse->getMessages()[0]->getDescription() . "\n";

            header( 'Location: /complete.php?transactionId=' . $tresponse->getTransId());
        }
        else
        {
         // echo "Transaction Failed \n";
          if($tresponse->getErrors() != null)
          {
           // echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
           // echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";            

            header( 'Location: /complete.php?errorMsg=' . $tresponse->getErrors()[0]->getErrorText() . "(Code:" . $tresponse->getErrors()[0]->getErrorText() + ")");

          }

            header( 'Location: /complete.php');
        }
      }
      else
      {
     //   echo "Transaction Failed \n";
        $tresponse = $response->getTransactionResponse();
        if($tresponse != null && $tresponse->getErrors() != null)
        {
         // echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
         // echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
           header( 'Location: /complete.php?errorMsg=' . $tresponse->getErrors()[0]->getErrorText() . "(Code:" . $tresponse->getErrors()[0]->getErrorText() + ")");                      
        }
        else
        {
         // echo " Error code  : " . $response->getMessages()->getMessage()[0]->getCode() . "\n";
          //echo " Error message : " . $response->getMessages()->getMessage()[0]->getText() . "\n";
              header( 'Location: /complete.php');
        }
          header( 'Location: /complete.php');
      }      
    }
    else
    {
     // echo  "No response returned \n";
          header( 'Location: /complete.php');
    }

    header( 'Location: /complete.php');

    */
?>
